import request from '@/utils/request'

export function getChangeDay(data) {
  return request({
    url: '/statistics/getChangeDay',
    method: 'post',
    data
  })
}

export function getInspectorDay(data) {
  return request({
    url: '/statistics/getInspectorDay',
    method: 'post',
    data
  })
}

export function getTypeDay(data) {
  return request({
    url: '/statistics/getTypeDay',
    method: 'post',
    data
  })
}
