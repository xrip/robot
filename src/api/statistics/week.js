import request from '@/utils/request'

export function getChangeWeek(data) {
  return request({
    url: '/statistics/getChangeWeek',
    method: 'post',
    data
  })
}

export function getInspectorWeek(data) {
  return request({
    url: '/statistics/getInspectorWeek',
    method: 'post',
    data
  })
}

export function getTypeWeek(data) {
  return request({
    url: '/statistics/getTypeWeek',
    method: 'post',
    data
  })
}
