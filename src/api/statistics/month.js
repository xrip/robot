import request from '@/utils/request'

export function getChangeMonth(data) {
  return request({
    url: '/statistics/getChangeMonth',
    method: 'post',
    data
  })
}

export function getInspectorMonth(data) {
  return request({
    url: '/statistics/getInspectorMonth',
    method: 'post',
    data
  })
}

export function getTypeMonth(data) {
  return request({
    url: '/statistics/getTypeMonth',
    method: 'post',
    data
  })
}
