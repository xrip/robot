import request from '@/utils/request'

export function listStrategy(data) {
  return request({
    url: '/detection/listStrategy',
    method: 'post',
    data
  })
}

export function addStrategy(data) {
  return request({
    url: '/detection/addStrategy',
    method: 'post',
    data
  })
}

export function removeStrategy(data) {
  return request({
    url: '/detection/removeStrategy',
    method: 'post',
    data
  })
}

export function modifyStrategy(data) {
  return request({
    url: '/detection/modifyStrategy',
    method: 'post',
    data
  })
}

export function getTaskByStrategyId(data) {
  return request({
    url: '/detection/getTaskByStrategyId',
    method: 'post',
    data
  })
}
