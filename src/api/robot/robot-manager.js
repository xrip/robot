import request from '@/utils/request'

export function listInfo(data) {
  return request({
    url: '/robot/listInfo',
    method: 'post',
    data
  })
}

export function modifyRobotName(data) {
  return request({
    url: '/robot/modifyRobotName',
    method: 'post',
    data
  })
}

export function bindMapInfo(data) {
  return request({
    url: '/robot/bindMapInfo',
    method: 'post',
    data
  })
}

export function modifyUser(data) {
  return request({
    url: '/app/modifyUser',
    method: 'post',
    data
  })
}
