import request from '@/utils/request'

export function getConfig(data) {
  return request({
    url: '/robot/getWebConfig',
    method: 'post',
    data
  })
}


export function saveConfig(data) {
  return request({
    url: '/robot/saveConfig',
    method: 'post',
    data
  })
}
