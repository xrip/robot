import request from '@/utils/request'

export function removeRole(data) {
  return request({
    url: '/role/removeRole',
    method: 'post',
    data
  })
}

export function listRole(data) {
  return request({
    url: '/role/listRole',
    method: 'post',
    data
  })
}

export function saveMenuByRoleId(data) {
  return request({
    url: '/role/saveMenuByRoleId',
    method: 'post',
    data
  })
}
export function addRole(data) {
  return request({
    url: '/role/addRole',
    method: 'post',
    data
  })
}

export function modifyRole(data) {
  return request({
    url: '/role/modifyRole',
    method: 'post',
    data
  })
}

export function treeMenu(data) {
  return request({
    url: '/role/treeMenu',
    method: 'post',
    data
  })
}

export function listMenuByRoleId(data) {
  return request({
    url: '/role/listMenuByRoleId',
    method: 'post',
    data
  })
}
