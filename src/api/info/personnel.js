import request from '@/utils/request'

export function addInspector(data) {
  return request({
    url: '/info/addInspector',
    method: 'post',
    data
  })
}

export function modifyInspector(data) {
  return request({
    url: '/info/modifyInspector',
    method: 'post',
    data
  })
}

export function removeInspector(data) {
  return request({
    url: '/info/removeInspector',
    method: 'post',
    data
  })
}

export function listInspector(data) {
  return request({
    url: '/info/listInspector',
    method: 'post',
    data
  })
}

export function listAllocInfoByInspectorId(data) {
  return request({
    url: '/info/listAllocInfoByInspectorId',
    method: 'post',
    data
  })
}
