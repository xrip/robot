import request from '@/utils/request'

export function listLocale(data) {
  return request({
    url: '/info/listLocale',
    method: 'post',
    data
  })
}

export function addLocale(data) {
  return request({
    url: '/info/addLocale',
    method: 'post',
    data
  })
}

export function modifyLocale(data) {
  return request({
    url: '/info/modifyLocale',
    method: 'post',
    data
  })
}

export function removeLocale(data) {
  return request({
    url: '/info/removeLocale',
    method: 'post',
    data
  })
}
