import request from '@/utils/request'

export function listDuty(data) {
  return request({
    url: '/info/listDuty',
    method: 'post',
    data
  })
}

export function addDuty(data) {
  return request({
    url: '/info/addDuty',
    method: 'post',
    data
  })
}

export function removeDuty(data) {
  return request({
    url: '/info/removeDuty',
    method: 'post',
    data
  })
}

export function modifyDuty(data) {
  return request({
    url: '/info/modifyDuty',
    method: 'post',
    data
  })
}

export function listDutyAll(data) {
  return request({
    url: '/info/listDutyAll',
    method: 'post',
    data
  })
}
