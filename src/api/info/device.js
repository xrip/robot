import request from '@/utils/request'

export function listDevice(data) {
  return request({
    url: '/info/listDevice',
    method: 'post',
    data
  })
}

export function addDevice(data) {
  return request({
    url: '/info/addDevice',
    method: 'post',
    data
  })
}

export function modifyDevice(data) {
  return request({
    url: '/info/modifyDevice',
    method: 'post',
    data
  })
}

export function removeDevice(data) {
  return request({
    url: '/info/removeDevice',
    method: 'post',
    data
  })
}

export function countDevice(data) {
  return request({
    url: '/info/countDevice',
    method: 'post',
    data
  })
}
