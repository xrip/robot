import request from '@/utils/request'

export function allocWork(data) {
  return request({
    url: '/alloc/allocWork',
    method: 'post',
    data
  })
}
