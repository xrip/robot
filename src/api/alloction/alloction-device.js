import request from '@/utils/request'

export function listUnAllocDeviceByLocaleId(data) {
  return request({
    url: '/alloc/listUnAllocDeviceByLocaleId',
    method: 'post',
    data
  })
}

export function listAllocDeviceByLocaleId(data) {
  return request({
    url: '/alloc/listAllocDeviceByLocaleId',
    method: 'post',
    data
  })
}

export function allocDevice(data) {
  return request({
    url: '/alloc/allocDevice',
    method: 'post',
    data
  })
}
