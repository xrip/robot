import request from '@/utils/request'

export function queryPoint(data) {
  return request({
    url: '/map/listSite',
    method: 'post',
    data
  })
}

export function editPointDone(data) {
  return request({
    url: '/map/modifySite',
    method: 'post',
    data
  })
}

export function removePointDone(data) {
  return request({
    url: '/map/removeSite',
    method: 'post',
    data
  })
}

export function addPointDone(data) {
  return request({
    url: '/map/addSite',
    method: 'post',
    data
  })
}

export function listRobot(data) {
  return request({
    url: '/robot/listRobotByMapId',
    method: 'post',
    data
  })
}

export function mapPreview() {
  return `${process.env.VUE_APP_BASE_API}` + '/map/preview'
}

export function getConfig() {
  return request({
    url: '/robot/getWebConfig',
    method: 'post'
  })
}

export function listMapAll() {
  return request({
    url: '/map/listMapAll',
    method: 'post'
  })
}

export function enableMapInfo(data) {
  return request({
    url: '/map/enableMapInfo',
    method: 'post',
    data
  })
}
