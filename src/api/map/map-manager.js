import request from '@/utils/request'

export function listMapInfo(data) {
  return request({
    url: '/map/listMapInfo',
    method: 'post',
    data
  })
}

export function modifyMapInfo(data) {
  return request({
    url: '/map/modifyMapInfo',
    method: 'post',
    data
  })
}

export function removeMapInfo(data) {
  return request({
    url: '/map/removeMapInfo',
    method: 'post',
    data
  })
}

export function listRobotByTaskId(data) {
  return request({
    url: '/task/listRobotByTaskId',
    method: 'post',
    data
  })
}

export function saveRobotByTaskId(data) {
  return request({
    url: '/task/saveRobotByTaskId',
    method: 'post',
    data
  })
}

export function listRobot(data) {
  return request({
    url: '/robot/listRobotByMapId',
    method: 'post',
    data
  })
}

export function addTask(data) {
  return request({
    url: '/task/addTask',
    method: 'post',
    data
  })
}
