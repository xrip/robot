import request from '@/utils/request'

export function listTask(data) {
  return request({
    url: '/task/listTask',
    method: 'post',
    data
  })
}

export function listSite(data) {
  return request({
    url: '/map/listSite',
    method: 'post',
    data
  })
}

export function listRobotByTaskId(data) {
  return request({
    url: '/task/listRobotByTaskId',
    method: 'post',
    data
  })
}

export function saveRobotByTaskId(data) {
  return request({
    url: '/task/saveRobotByTaskId',
    method: 'post',
    data
  })
}

export function listRobot(data) {
  return request({
    url: '/robot/listRobotByMapId',
    method: 'post',
    data
  })
}

export function removeTask(data) {
  return request({
    url: '/task/removeTask',
    method: 'post',
    data
  })
}

export function addTask(data) {
  return request({
    url: '/task/addTask',
    method: 'post',
    data
  })
}

export function modifyTask(data) {
  return request({
    url: '/task/modifyTask',
    method: 'post',
    data
  })
}
