import request from '@/utils/request'

export function listEvent(data) {
  return request({
    url: '/event/listEvent',
    method: 'post',
    data
  })
}

export function modifyEvent(data) {
  return request({
    url: '/event/modifyEvent',
    method: 'post',
    data
  })
}
