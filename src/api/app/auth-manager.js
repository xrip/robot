import request from '@/utils/request'

export function listUser(data) {
  return request({
    url: '/app/listUser',
    method: 'post',
    data
  })
}

export function removeUser(data) {
  return request({
    url: '/app/removeUser',
    method: 'post',
    data
  })
}

export function addUser(data) {
  return request({
    url: '/app/addUser',
    method: 'post',
    data
  })
}

export function modifyUser(data) {
  return request({
    url: '/app/modifyUser',
    method: 'post',
    data
  })
}
