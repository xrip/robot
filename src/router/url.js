import Layout from '@/layout'

export const url = new Map()
// 用户
url.set('user', Layout)
url.set('userManager', () => import('@/views/user/user-manager/index'))
url.set('roleManager', () => import('@/views/user/role-manager/index'))
url.set('menuManager', () => import('@/views/user/menu-manager/index'))
// 机器人
url.set('robot', Layout)
url.set('robotConfig', () => import('@/views/robot/robot-config/index'))
url.set('robotManager', () => import('@/views/robot/robot-manager/index'))
// 地图
url.set('map', Layout)
url.set('siteEdit', () => import('@/views/mapEditor/editor'))
url.set('mapManager', () => import('@/views/map/map-manager/index'))
// 应用
url.set('app', Layout)
url.set('authManager', () => import('@/views/app/auth-manager/index'))
// 任务
url.set('taskManager', () => import('@/views/map/task-manager/index'))
// 信息维护
url.set('info', Layout)
url.set('device', () => import('@/views/info/device/index'))
url.set('monitorPoint', () => import('@/views/info/monitorPoint/index'))
url.set('personnel', () => import('@/views/info/personnel/index'))
url.set('duty', () => import('@/views/info/duty/index'))
// 分配管理
url.set('allocation', Layout)
url.set('deviceAllocation', () => import('@/views/allocation/device-allocation/index'))
url.set('workAllocation', () => import('@/views/allocation/work-allocation/index'))
//  检测管理
url.set('detection', Layout)
url.set('strategyManager', () => import('@/views/detection/strategy-manager/index'))
url.set('scheduleTask', () => import('@/views/detection/schedule-task/index'))
//  事件管理
url.set('event', Layout)
url.set('eventHandle', () => import('@/views/event/event-handle/index'))
//  数据统计
url.set('statistics', Layout)
url.set('dayStatistics', () => import('@/views/statistics/day-statistics/index'))
url.set('weekStatistics', () => import('@/views/statistics/week-statistics/index'))
url.set('monthStatistics', () => import('@/views/statistics/month-statistics/index'))
