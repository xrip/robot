// 定义地图对象
function pictureMap(options, that) {
  that.viewDivId = options.viewId
  that.markerHeight = 6
  that.markerWidth = 6
  that.markerStart = options.markerStart
  that.markerImg = options.markerImg || '<i class="el-icon-s-custom" style="color: red"></i>'
  that.uploadFun = options.uploadFun || function(data) { return }
  that.mapImg = options.mapImg || ''
  that.clickFun = options.clickFun || function(data) { return }
  that.markerElement = {}
  that.popUpWidth = options.popUpWidth || 50
  that.popUpHeight = options.popUpHeight || 50
  that.popContent = options.popContent || document.createElement('div')
  that.isEditor = options.isEditor || false
  that.originX = options.originX || -50
  that.originY = options.originY || -50
  that.pictureMapId = options.pictureMapId || 0
  that.resolution = options.resolution
  that.originalWidth = options.width
  that.originalHeight = options.height
  that.realWidth = that.originalWidth * parseFloat(that.resolution)
  that.realHeight = that.originalHeight * parseFloat(that.resolution)
  that.isRealSize = options.isRealSize

  // 初始化
  function mapInit(isRealSize) {
    that.viewDiv = document.getElementById(that.viewDivId)
    that.viewDiv.innerHTML = ''
    that.dv = document.createElement('div')
    that.dv.id = 'pictureMapDiv'
    // that.dv.style.width = "300%";
    // that.dv.style.paddingBottom = "300%";
    that.dv.style.width = that.originalWidth + 'px'
    that.dv.style.height = that.originalHeight + 'px'
    that.dv.style.top = topPercent(that.viewDiv.offsetHeight, that.originalHeight) + '%'
    that.dv.style.left = leftPercent(that.viewDiv.offsetWidth, that.originalWidth) + '%'
    that.dv.style.position = 'absolute'
    that.dv.style.backgroundImage = "url('" + that.mapImg + "')"
    that.dv.style.backgroundSize = '100% 100%'
    that.dv.style.backgroundRepeat = 'nono-repeat'
    that.viewDiv.appendChild(that.dv)
    that.mapHeight = that.originalHeight
    that.mapWidth = that.originalWidth
    // that.dv.style.transform = "scale(2,2)";
    that.dv.style.transform = 'scale(1,1)'
    that.scale = 1
    addMapPanEvent()
    cancelContextMenu()
    if (that.isEditor) {
      addPOIPoint()
    }
    addClickEvent()
  }
  mapInit(that.isRealSize)

  // 地图平移pan
  function addMapPanEvent() {
    var dv = that.dv
    var x = 0
    var y = 0
    var l = 0
    var t = 0
    var isDown = false

    // 鼠标按下事件
    dv.onmousedown = function(e) {
      // 获取x坐标和y坐标
      x = e.clientX
      y = e.clientY

      // 获取左部和顶部的偏移量
      l = dv.offsetLeft
      t = dv.offsetTop
      // 开关打开
      isDown = true
    }
    // 鼠标移动
    dv.onmousemove = function(e) {
      if (isDown === false) {
        return
      }
      // 获取x和y
      var nx = e.clientX
      var ny = e.clientY
      // 计算移动后的左偏移量和顶部的偏移量
      var nl = nx - (x - l)
      var nt = ny - (y - t)

      dv.style.left = nl + 'px'
      dv.style.top = nt + 'px'

      // 设置样式
      dv.style.cursor = 'move'
    }
    // 鼠标抬起事件
    dv.onmouseup = function() {
      // 开关关闭
      isDown = false
      dv.style.cursor = 'default'
    }
  }

  function addZoomEvent() {
    var dv = that.dv
    dv.onmousewheel = function(e) {
      if (e.wheelDelta > 0) { // 向上滚动
        if (that.scale + 0.1 >= 2) {
          return
        }
        that.scale += 0.1
        dv.style.transform = 'scale(' + that.scale + ',' + that.scale + ')'
      } else { // 向下滚动
        if (that.scale - 0.1 < 0) {
          return
        }
        that.scale -= 0.1
        dv.style.transform = 'scale(' + that.scale + ',' + that.scale + ')'
      }
    }
  }

  function cancelContextMenu() {
    that.dv.oncontextmenu = function(e) {
      e.preventDefault()
    }
  }

  function addPOIPoint() {
    var dv = that.dv
    dv.oncontextmenu = function(e) {
      e.preventDefault()
      if (e.target.tagName === 'I' || e.target.id.indexOf('img') > -1) {
        return
      }
      // 执行代码块
      // 计算鼠标偏移中心点的偏移量
      var offsetX = e.offsetX - that.mapWidth / 2// 鼠标距离中心点的偏移量
      var offsetY = -e.offsetY + that.mapHeight / 2// 鼠标距离中心点的偏移量

      // console.log(e.offsetX/that.mapWidth+"-,"+e.offsetY/that.mapHeight);
      // debugger

      // 计算中心点位置
      var currentMapHeight = dv.getBoundingClientRect().height
      var currentMapWidth = dv.getBoundingClientRect().width
      var centerX = currentMapWidth / 2
      var centerY = currentMapHeight / 2

      // 计算点相对位置（小数值）
      var rLength = (centerX + offsetX * that.scale) / currentMapWidth
      var cLength = (centerY + offsetY * that.scale) / currentMapHeight

      console.log(rLength + ',------' + cLength)

      // 创建POI点，并添加到图上点击位置
      var ele = document.createElement('div')
      ele.style.position = 'absolute'
      ele.style.bottom = cLength * 100 + '%'
      ele.style.left = rLength * 100 + '%'
      ele.style.height = that.markerHeight + 'px'
      ele.style.width = that.markerWidth + 'px'
      ele.style.marginTop = -(that.markerHeight) + 'px'
      ele.style.marginLeft = -that.markerWidth / 2 + 'px'
      if (e.target.childNodes.length === 0) {
        ele.innerHTML = that.markerStart
      } else {
        ele.innerHTML = that.markerImg
      }
      ele.getElementsByTagName('i')[0].style.transform = 'scale(1.0,1.0)'// 新的
      ele.getElementsByTagName('i')[0].style.position = 'absolute'
      ele.getElementsByTagName('i')[0].style.marginTop = ' -10px'
      ele.getElementsByTagName('i')[0].style.marginLeft = '-5px'
      // ele.style.backgroundImage = "url('"+that.markerImg+"')";旧的
      // ele.style.backgroundSize = "100% 100%";
      // ele.style.backgroundRepeat = "no-repeat";

      // var dateId = getCurrentDate();//数据id
      // ele.id = 'img'+dateId;

      // var x = rLength*409.6 - (409.6 + parseFloat(that.originX)) -1;
      // var y = -cLength*409.6 - parseFloat(that.originY) -1;
      var x = rLength * that.realWidth + parseFloat(that.originX)
      var y = cLength * that.realHeight + parseFloat(that.originY)

      ele.id = ('img' + x.toString() + y.toString()).replace(/[&\|\\\*^%$#@\-|\.]/g, '')
      console.log(x + ',' + y)

      ele.dataContent = { // 数据内容
        mapId: that.pictureMapId,
        name: '',
        yaw: '',
        // top:cLength*100,
        // left:rLength*100,
        x: x,
        y: y,
        lt: x + ',' + y,
        parentDiv: dv,
        ele: ele
      }

      // that.markerElement = ele;
      // popUpEvent();
      dv.appendChild(ele)// 弹窗取消需将该图标删除
      addBeforeContent(ele.id, '')
      // document.styleSheets[0].insertRule("#img"+dateId +'::before { content: ""; width: 60px;height: 12px;line-height: 12px;text-align: center;position: absolute;margin-left: -28px; margin-top: -8px;font-size: 0.04px;transform: scale(0.3);}', 0);
      that.uploadFun(ele.dataContent)// 弹窗确定后调用该函数，函数中可定义接口上传dataContent
    }
  }

  // 点击事件
  function addClickEvent() {
    that.dv.addEventListener('click', function(e) {
      var dataContent = e.target.dataContent
      that.clickFun(dataContent)
    })
  }

  // 弹窗
  function popUpEvent() {
    var popUp = document.getElementById('popUpDiv')
    popUp.style.height = that.popUpHeight + 'px'
    popUp.style.width = that.popUpWidth + 'px'
    popUp.style.marginTop = -(that.popUpHeight + that.markerHeight) + 'px'
    popUp.style.marginLeft = -that.popUpWidth / 2 + 'px'
    popUp.style.backgroundColor = 'blue'
    popUp.style.top = that.markerElement.dataContent.top
    popUp.style.left = that.markerElement.dataContent.left

    popUp.appendChild(that.popContent)
    var popButtonDiv = document.createElement('div')
    popButtonDiv.innerHTML = "<div id='popCancel' style='font-size: 1px;height: 11px;display: inline;color: darkkhaki;bottom: 5px;position: absolute;cursor: pointer;'>取消</div>" +
            "<div id='popConform' style='    font-size: 8px;height: 11px;display: inline;bottom: 5px;position: absolute; margin-left: 25px;color: darkkhaki;cursor: pointer;'>确定</div>"
    popUp.appendChild(popButtonDiv)

    popUp.style.display = 'block'
  }

  // 返回对象
  function Map() {
    that.viewId = that.viewDivId
  }

  Map.zoomPlus = function() {
    if (that.scale + 0.1 >= 2) {
      return
    }
    that.scale += 0.1
    that.dv.style.transform = 'scale(' + that.scale + ',' + that.scale + ')'
  }

  Map.zoom = function(scale) {
    if (scale >= 2 || scale <= 0) {
      return
    }
    that.dv.style.transform = 'scale(' + scale + ',' + scale + ')'
  }

  Map.zoomReduce = function() {
    if (that.scale - 0.1 < 0) {
      return
    }
    that.scale -= 0.1
    that.dv.style.transform = 'scale(' + that.scale + ',' + that.scale + ')'
  }

  // 调用该方法批量导入POI点数据
  Map.addPOIPoints = function(data, isRobot) {
    data.map(function(item) {
      //  var item = t.dataContent;
      var bottom = ((typeof (item.y) === 'string' ? parseFloat(item.y) : item.y) - parseFloat(that.originY)) * 100 / that.realHeight
      var left = ((typeof (item.x) === 'string' ? parseFloat(item.x) : item.x) - parseFloat(that.originX)) * 100 / that.realWidth

      console.log('name=' + item.name + 'x=' + left + '----' + 'y=' + bottom)
      var ele = document.createElement('div')
      ele.style.position = 'absolute'
      ele.style.left = left + '%'
      ele.style.bottom = bottom + '%'
      ele.style.height = that.markerHeight + 'px'
      ele.style.width = that.markerWidth + 'px'
      ele.style.marginTop = -(that.markerHeight - 1) + 'px'
      ele.style.marginLeft = -that.markerWidth / 2 + 'px'
      if (item.start) {
        ele.innerHTML = that.markerStart
      } else {
        ele.innerHTML = that.markerImg
      }
      ele.getElementsByTagName('i')[0].style.transform = 'scale(1.0,1.0)'
      ele.getElementsByTagName('i')[0].style.position = 'absolute'
      ele.getElementsByTagName('i')[0].style.marginTop = ' -10px'
      ele.getElementsByTagName('i')[0].style.marginLeft = '-5px'
      ele.dataContent = item
      if (isRobot) {
        const childs = that.dv.childNodes
        for (var i = 0; i < childs.length; i++) {
          // eslint-disable-next-line eqeqeq
          if (childs[i].name != undefined && 'robot' + item.id === childs[i].name) {
            that.dv.removeChild(childs[i])
          }
        }
        ele.name = 'robot' + item.id
      }
      ele.id = ('img' + item.x.toString() + item.y.toString()).replace(/[&\|\\\*^%$#@\-|\.]/g, '')
      that.dv.appendChild(ele)
      addBeforeContent(ele.id, item.name)
    })
  }

  Map.removePoint = function(mapId) {
    that.dv.children.forEach(function(item) {
      const itemId = (item.dataContent.x.toString() + item.dataContent.y.toString()).replace(/[&\|\\\*^%$#@\-|\.]/g, '')
      if (itemId === mapId) {
        item.remove()
      }
    })
  }

  Map.removePoints = function() {
    that.dv.innerHTML = ''
  }

  Map.getScale = function() {
    return that.scale
  }

  function addBeforeContent(mapId, name, scale) {
    document.styleSheets[0].insertRule('#' + mapId + '::before { content: "' + name + '"; width: 60px;height: 12px;line-height: 12px;text-align: center;position: absolute;margin-left: -27px; margin-top: -20px;font-size: 0.04px;transform: scale(' + 0.90 + ');}', 0)
  }

  function addAfterContent(mapId, img) {
    document.styleSheets[0].insertRule('#' + mapId + '::after { content: "\\F401";font-family: Ionicons;}', 0)
  }

  function leftPercent(boxWith, realWith) {
    if (boxWith >= realWith) {
      return (boxWith - realWith) / 2 / boxWith * 100
    } else {
      return -(realWith - boxWith) / 2 / boxWith * 100
    }
  }

  function topPercent(boxHeight, realHeight) {
    if (boxHeight >= realHeight) {
      return (boxHeight - realHeight) / 2 / boxHeight * 100
    } else {
      return -(realHeight - boxHeight) / 2 / boxHeight * 100
    }
  }

  Map.updateContent = function(mapId, name) {
    document.styleSheets[0].cssRules.forEach(function(t, index) {
      if (t.selectorText === '#img' + mapId + '::before') {
        document.styleSheets[0].removeRule(index)
        addBeforeContent('img' + mapId, name)
      }
    })
  }

  return Map
}
export default pictureMap
