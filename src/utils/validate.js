/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * 校验用户名
 * @param rule
 * @param value
 * @param callback
 */
export function validUsername(rule, value, callback) {
  const reg = /^[a-zA-Z0-9]{1,26}$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    return callback(new Error('账号不能为空'))
  }
  if (!reg.test(value)) {
    console.log(value)
    return callback(new Error('请输入1-26位字母和数字组成的用户名'))
  } else {
    callback()
  }
}
/**
 * 校验密码
 * @param rule
 * @param value
 * @param callback
 */
export function validPassword(rule, value, callback) {
  const reg = /^[a-zA-Z0-9]{6,18}$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    return callback(new Error('密码不能为空'))
  }
  if (!reg.test(value)) {
    console.log(value)
    return callback(new Error('请输入6-18位字母和数字组成的密码'))
  } else {
    callback()
  }
}

/**
 * 校验非必填密码
 * @param rule
 * @param value
 * @param callback
 */
export function validNotRequiredPassword(rule, value, callback) {
  const reg = /^[a-zA-Z0-9]{6,18}$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    callback()
  }
  if (!reg.test(value)) {
    console.log(value)
    return callback(new Error('请输入6-18位字母和数字组成的密码'))
  } else {
    callback()
  }
}

/**
 * 校验手机号
 * @param rule
 * @param value
 * @param callback
 */
export function validRequiredPhoneNum(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    return callback(new Error('手机号不能为空'))
  }
  if (!reg.test(value)) {
    return callback(new Error('请输入正确的手机号'))
  } else {
    callback()
  }
}
/**
 * 校验手机号
 * @param rule
 * @param value
 * @param callback
 */
export function validPhoneNum(rule, value, callback) {
  const reg = /^[1][3,4,5,7,8,9][0-9]{9}$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    callback()
  }
  if (!reg.test(value)) {
    return callback(new Error('请输入正确的手机号'))
  } else {
    callback()
  }
}

/**
 * 校验任务执行轨迹
 * @param rule
 * @param value
 * @param callback
 */
export function validTaskNode(rule, value, callback) {
  if (value && value.length >= 2) {
    callback()
  } else {
    return callback(new Error('执行轨迹至少2个节点'))
  }
}

/**
 * 校验不能为空
 * @param rule
 * @param value
 * @param callback
 */
export function validNotNull(rule, value, callback) {
  if (value === '' || value === undefined || value === null) {
    return callback(new Error(rule.name + '不能为空'))
  }
  callback()
}

/**
 * @param {string} url
 * @returns {Boolean}
 */
export function validURL(url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/**
 * @param {string} value
 * @returns {Boolean}
 */
export function validAlphabets(rule, value, callback) {
  const reg = /^[A-Za-z]+$/
  if (value === '' || value === undefined || value === null || value.split(' ').join('').length === 0) {
    return callback(new Error('英文内容不能为空'))
  }
  if (!reg.test(value)) {
    return callback(new Error('请输入正确的英文字符'))
  } else {
    callback()
  }
}

/**
 * @param {string} email
 * @returns {Boolean}
 */
export function validEmail(email) {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return reg.test(email)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function isString(str) {
  if (typeof str === 'string' || str instanceof String) {
    return true
  }
  return false
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isArray(arg) {
  if (typeof Array.isArray === 'undefined') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return Array.isArray(arg)
}
